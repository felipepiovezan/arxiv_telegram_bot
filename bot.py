import rss_parser
import sys
import telepot 
import time

def entry_to_telegram_msg(item):
    msg = item.url + '\n'
    msg += item.name + '\n'
    msg += ', '.join(item.authors)
    msg += '\n' + item.abstract
    return msg

# distributted computing announcement group
DC_ID = -1001134064638

if len(sys.argv) < 2:
    print("Incorrect usage, missing key argument: python gn_bot.py BOT_KEY")
    exit(0)
bot = telepot.Bot(sys.argv[1])

with open('last_hash.txt') as f:
    last_hash = int(f.readline())


while(True):
    hash, updates = rss_parser.get()
    if hash != last_hash:
        for item in updates:
           bot.sendMessage(DC_ID, "``` " + entry_to_telegram_msg(item) 
                      + "```", "Markdown")
        bot.sendMessage(DC_ID, "=======================")
        last_hash = hash
        with open('last_hash.txt', 'w') as f:
            f.write(str(last_hash))

    time.sleep(1)
