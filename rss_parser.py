import urllib.request
import xml.etree.ElementTree as ET
from collections import namedtuple
import hashlib

ITEM_TAG = '{http://purl.org/rss/1.0/}item'

FeedEntry = namedtuple('FeedEntry', ['name', 'url', 'authors', 'abstract'])

rss_url = 'http://export.arxiv.org/rss/cs.DC'


def get():
    hasher = hashlib.sha256()
    feed = urllib.request.urlopen(rss_url)
    root = ET.parse(feed).getroot()
    entries = []

    for item in root: 
        if item.tag != ITEM_TAG:
            continue
        name = item[0].text
        name = name[:name.find('.')]
        url = item[1].text
        abstract = item[2].text.replace('<p>', '').replace('</p>', '')
        abstract = abstract.replace('\n', ' ')
        authors = item[3].text.split(',')
        for i, author in enumerate(authors):
            authors[i] = author[author.find('">') + 2 : author.find('</a')]
            authors[i] = authors[i]
        authors = frozenset(authors)
        new_entry = FeedEntry(name, url, authors, abstract)
        entries += [new_entry]
        hasher.update(abstract.encode('utf-8'))
    
    return int(hasher.hexdigest(), 16), entries


